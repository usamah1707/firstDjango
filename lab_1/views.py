from django.shortcuts import render
from datetime import date
import datetime


# Enter your name here
mhs_name = "Usamah Nashirulhaq" 

thisYear = date.today().year
birth_date = datetime.date(1998, 7, 17)



# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age':calculate_age(1998)}
    return render(request, 'index.html', response)


def calculate_age(birth_year):
	return thisYear-birth_year
	

