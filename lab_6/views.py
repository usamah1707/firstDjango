from django.shortcuts import render
from django.http import HttpResponseRedirect

response = {}
def index(request):
	response['author'] = "Usamah Nashirulhaq"
	html = 'lab_6/lab_6.html'
	return render(request, html, response)
